#ifndef SSLBASE_H
#define SSLBASE_H

#include <QObject>
#include <QNetworkInterface>
#include <QUdpSocket>
#include <QTimer>
#include <QDebug>

#include "src/Field/field.h"

#include "ssl_simulation_robot_control.pb.h"
#include "ssl_simulation_robot_feedback.pb.h"
#include "ssl_simulation_control.pb.h"
#include "ssl_simulation_error.pb.h"
#include "ssl_gc_common.pb.h"
#include "ssl_simulation_config.pb.h"
#include "ssl_wrapper.pb.h"
#include "ssl_vision_geometry.pb.h"
#include "ssl_vision_detection.pb.h"

#define VISION_PORT 10020
#define VISION_IP "224.5.23.2"
#define CONTROL_PORT 20011
#define BLUE_PORT 10301
#define YELLOW_PORT 10302

#define CYCLE_MS 1000/60 // 60 Hz

class SSLBase : public QObject
{
    Q_OBJECT

    QUdpSocket *udpVision;
    QUdpSocket *udpTeam;
    QHostAddress haSimulatorIP;
    int iTeamPort;
    QTimer *tmrLoop;

    bool bPacketsReceived;
    int iInterfaceIndex;
    int iCycles;

    Field field;

    void vDiscoverInterface();
    void vSendData();
    void vUpdateVisionData(const SSL_WrapperPacket &_visionData);

public:
    SSLBase();
    ~SSLBase();

    void vConnect();
    void vStart();
    void vMain();

    //----------------------------------------------------------Funcoes Artilheiro infinito

    void atacante_segue_bola(int ID_Robo);
    float alinha_robo(int ID_Robo, QPoint pos_Alinhamento);
    int verifica_Goleiro();
    int verifica_Distancia_Robos(int ID_Robo, QPoint pos, float dist, int aliados);
    int robo_posicionou = 0;
    int robo_posicionou_2 = 0;
    int ja_Chutou = 0;
    int aleatorio = 0;
    //----------------------------------------------------------Funcoes goleiro

    void Estado_da_Bola();
    void chutador();
    void goleiro();
    float    x_LinhaGol   =    4084;


    QVector<QVector2D> posicao_bola;

private slots:
    void vParseVisionPacket();
    void vParseRobotFeedback();
    void vUpdate();
};

#endif // SSLBASE_H
