#include "src/SSLBase/sslbase.h"
#include <QVector>
#include <QVector2D>


//-------------------------------------------------------------------------------VERIFICANDO ESTADO DA BOLA


void SSLBase::Estado_da_Bola()//ESTA FUNCAO EH UM FRAGMENTO QUE IRA COMPOR O CODIGO FINAL DO GOLEIRO
{
    for(int i = 0; i<=5; i++)//verificando ID do time
    {
        if(field.allies.robots.value(i)->bGetBallSensor() == 0)
        {
            qDebug() << "Bola esta SOZINHA";
            qDebug() << "Alinhar Goleiro";

            field.allies.robots.value(1)->vMoveTo(QPoint(x_LinhaGol, field.ball.ptGetPosition().y()),
                                                  field.ball.ptGetPosition(), 2);
            //                                    float,      int, ?
            //                                    Qpoint posicao onde o robo tem que ir
            //                                                Qpoint/float angulo do robo
            //                                                    float velocidade

        }
        else if(field.allies.robots.value(i)->bGetBallSensor() == 1)
        {
            qDebug() << "Bola esta com o inimiigo";
            if(y_goleiro < 500 && y_goleiro > -500)
            {
                qDebug() << "Inimigo esta mirando no Gol!!!!";
                qDebug() << "Goleiro -> INTERSECCAO";
            }
            else
            {
                qDebug() << "Inimigo nao mirando no gol";
                qDebug() << "Alinhar Goleiro";

                field.allies.robots.value(1)->vMoveTo(QPoint(x_LinhaGol, field.ball.ptGetPosition().y()),
                                                      field.ball.ptGetPosition(), 2);

            }

        }

    }
}

void SSLBase::goleiro()
{




    if(field.geometry.retGetField().size().width() > 0)//depois testar apahar esta linha
    {
        //linha do gol
        for(QPoint goalPoint = field.geometry.retGetPositiveGoal().topRight();
            goalPoint.y() >= field.geometry.retGetPositiveGoal().bottomRight().y();
            goalPoint.setY(goalPoint.y() - 50))

        {
            QVector2D
                    //VETOR gol -> bola  =>
                    Vetor_dist_GolxBola = QVector2D(goalPoint - field.ball.ptGetPosition()),

                    //VETOR robo-> bola
                    Vetor_dist_RoboxBola = QVector2D(field.opponents.robots.value(1)->ptGetPosition() - field.ball.ptGetPosition()),

                    //VETOR distancia
                    d = Vetor_dist_RoboxBola - (Vetor_dist_RoboxBola.dotProduct(Vetor_dist_RoboxBola, Vetor_dist_GolxBola) / Vetor_dist_GolxBola.lengthSquared()) * Vetor_dist_GolxBola;

            //Raio do Robo
            int Rrobo = 90;

            for(int i = 0; i<=5; i++)//calculando a distante de cada robo em relacao a bola
            {

                //Vetor distancia Robo x Bola


                QVector2D
                        VdistRxB = QVector2D (field.opponents.robots.value(i) -> ptGetPosition() - field.ball.ptGetPosition());
                //                    qDebug() << "Vetor distancia Robo x Bola = " <<VdistRxB.length() ;


                if( VdistRxB.length() < 120 && i == 1)//Se a distancia entre o robo  oponente e a bola for menor que 120...
                {
                    qDebug() << "robo esta com a bola";


                }
            }


        }

    }



    //-------------------------------------------------------------------------------Construindo reta Bola x Gol
    //float    x_LinhaGol   =    4084;

    int    x_ball       =   field.ball.ptGetPosition().x(),
           y_ball       =   field.ball.ptGetPosition().y();


    posicao_bola.append(QVector2D(x_ball, y_ball));

    int tamanho_Vetor = posicao_bola.size();

    if( tamanho_Vetor > 2 )
    {

        QVector2D  ultima_posicao        = posicao_bola.at(tamanho_Vetor - 1),
                   penultima_posicao     = posicao_bola.at(tamanho_Vetor - 2);

        float dist_deslocamento_bola     = ultima_posicao.distanceToPoint(penultima_posicao);
        qDebug() << dist_deslocamento_bola;

        if( dist_deslocamento_bola > 4)//Se a Bola estiver em movimento...
        {
            //Calcular o vetor da bola:
            QVector2D Vetor_bola = (ultima_posicao - penultima_posicao).normalized();

//-------------------------------------------------------------------------------Determinando coordenada Y interseccao posicao_bola x Pg

            float y_goleiro =  ultima_posicao.y() + Vetor_bola.y() * ((x_LinhaGol - ultima_posicao.x()) / Vetor_bola.x() );

            if (y_goleiro < 500 && y_goleiro > -500)
            {


                //-------------------------------------------------------------------------------Mover Goleiro para Interseccao x_LinhaGol X Reta Bola_Gol

                field.allies.robots.value(1)->vMoveTo({qRound(x_LinhaGol), qRound(y_goleiro)}, field.ball.ptGetPosition(), 3);

            }

        }

        else
        {
            field.allies.robots.value(1)->vMoveTo(field.allies.robots.value(1)->ptGetPosition() , field.ball.ptGetPosition(), 3);
        }
    }

}

void SSLBase::vMain()
{
    // Para acessar os objetos do campo (robôs, bola, geometria) utilize o
    // atributo field, por exemplo, para fazer o robô 0 ir até o ponto (0, 0)
    // mm, com ângulo pi rad, à uma velocidade máxima de 2m/s utilize:
    //
    //field.allies.robots.value(2)->vMoveTo(field.ball.ptGetPosition(), M_PI, 2);
    atacante_segue_bola(2);


    goleiro();
}

//===========================================================================================


























int iDmais_prox()
{
    if( field.ball.ptGetPosition().x() > 0)
    {
     int Mais_prox = 2000;
     for(int AM = 0; AM <= 5; AM++)
     {


         int ID_eq = -1;
         for(int  AZ = 0; AZ <= 5; AZ++)
         {
             QVector2D
                     vAMxAZ = QVector2D(field.opponents.robots.value(AM) -> ptGetPosition() - field.opponents.robots.value(AZ) -> ptGetPosition() );
             //qDebug()<< "roboAZ "<<AZ<< "roboAM "<<AM<<"dist "<< vAMxAZ;

             float distancia = qSqrt( qPow(vAMxAZ.x(),2) + qPow(vAMxAZ.y(),2));
             //qDebug() << "dist: " << distancia;

             if(distancia < Mais_prox)
             {
                 Mais_prox = distancia;
                 ID_eq = AZ;
             }

         }

         QVector2D
                 VRxB = QVector2D (field.opponents.robots.value(AM) -> ptGetPosition() - field.ball.ptGetPosition());
         //                    qDebug() << "Vetor distancia Robo x Bola = " <<VdistRxB.length() ;

     }





    }
        qDebug() << "oi";
}
//-------------------------------------------------------------------------------

int iy_golerio()
{
    if(d.length() > Rrobo)//GOL ABERTO
    {
        //-------------------------------------------Reta do chute

        int x_ball  = field.ball.ptGetPosition().x(),
            y_ball  = field.ball.ptGetPosition().y(),
            x_goalP = goalPoint.x(),
            y_goalP = goalPoint.y();

        qDebug() << x_ball << "; " << y_ball << "; " << x_goalP << "; " << y_goalP << "; ";
        //Equacao Reta do chute

        int a_chute = ( y_ball - y_goalP) / (x_ball - x_goalP ),
            b_chute =  y_ball - a_chute * x_ball ;


        //Interseccao linha de chute x goleiro

        int y_goleiro = a_chute * 4084 + b_chute;


        field.allies.robots.value(1)-> bGetBallSensor();
        field.opponents.robots.value(1)->vMoveTo({4084, y_goleiro}, M_PI , 1);

        for(int i = 0; i<=5; i++)
        {
            //Vetor distancia Robo x Bola
            QVector2D
                    VdistRxB = QVector2D (field.opponents.robots.value(i) -> ptGetPosition() - field.ball.ptGetPosition());
            //                    qDebug() << "Vetor distancia Robo x Bola = " <<VdistRxB.length() ;
            if( VdistRxB.length() < 120 && i == 1)
            {
                QVector2D
                        VaR1xR2 = QVector2D((field.opponents.robots.value(2)->ptGetPosition()) - (field.opponents.robots.value(1)->ptGetPosition())),
                        VbR1xR2 = QVector2D( qCos(field.opponents.robots.value(2) -> fGetAngle()), qSin(field.opponents.robots.value(2) -> fGetAngle()));

                float
                        DeltaR1xR2 = qAcos(VaR1xR2.dotProduct(VaR1xR2, VbR1xR2) / VaR1xR2.length() * VbR1xR2.length()) ;

                //convertendo Radianos para  Graus
                DeltaR1xR2 = qRadiansToDegrees(DeltaR1xR2);

                if( VdistRxB.length() < 120 && i == 1)
                    //if(DeltaR1xR2 >= -3 && DeltaR1xR2 <= 3)
                {


                    //Robo 1 em posse da bola
                    qDebug() << "Robo com a posse da bola";
                    //Goleiro passa bola para jogador 4
                    field.opponents.robots.value(1)->vRotateTo(field.opponents.robots.value(4)->ptGetPosition());
                    field.opponents.robots.value(4)->vRotateTo(field.opponents.robots.value(1)->ptGetPosition());
                    field.opponents.robots.value(4)->vSetRollerSpeed(100);

                    float robotAngle = field.opponents.robots.value(1)->fGetAngle();

                    int error = General::dAngleV1V2(QVector2D(qCos(robotAngle), qSin(robotAngle)),
                                                    QVector2D(field.opponents.robots.value(4)->ptGetPosition() -
                                                              field.opponents.robots.value(1)->ptGetPosition()));

                    qDebug() << "RobotAngle" << robotAngle <<"\n angulo do 1: "<< field.opponents.robots.value(1)-> fGetAngle();

                    if(error <= 5) // Menos de 5 graus de erro
                    {
                        qDebug() << "Goleiro vai passar a bola!";
                        field.opponents.robots.value(1)->vKick( 3, DIRECT_KICK);

                    }

                }

            }
            //                 if(field.opponents.robots.value(1)->bGetBallSensor() == 1)

            //                 {//Passar bola para jogador 4
            //                    field.opponents.robots.value(1)->vRotateTo(field.opponents.robots.value(4) -> ptGetPosition());
            //                    field.opponents.robots.value(4)->vRotateTo(field.opponents.robots.value(1) -> ptGetPosition());
            //                    field.opponents.robots.value(4)->vSetRollerSpeed(100);
            //                    field.allies.robots.value(1)->vKick( 3, DIRECT_KICK);
            //                 }

            //                //ROBO DEVE SE MOVIMENTAR PARA COBRIR/ INTERCEPTAR LINHA DE CHUTE AO GOL

            //                //1.Dados de entrada:

            //                //-Ponto da Bola
            //                //-Ponto do Gol
            //                //-Ponto dos Robos aliados

            //                //( Mais proximo && DISPONIVEL para movimentacao )--->Verificar qual robo vai interceptar a linha de chute aparente

            //                //-Vetor gol -> bola
            //                //-Vetor robo -> bola
            //                //-Vetor distancia

            //field.allies.robots.value(0)->vMoveTo({1554, 0050}, M_PI , 2);

            //equacao da reta

            // x goleiro --> 4084 (FIXO)

        }
    }

}

//-------------------------------------------------------------------------------
void SSLBase::vMain()
{


    // Para acessar os objetos do campo (robôs, bola, geometria) utilize o
    // atributo field, por exemplo, para fazer o robô 0 ir até o ponto (0, 0)
    // mm, com ângulo pi rad, à uma velocidade máxima de 2m/s utilize:
    //
    //                   ID do robo/Mover para posicao:


    if(field.geometry.retGetField().size().width() > 0)
    {
        //linha do gol
        for(QPoint goalPoint = field.geometry.retGetPositiveGoal().topRight();
            goalPoint.y() >= field.geometry.retGetPositiveGoal().bottomRight().y();
            goalPoint.setY(goalPoint.y() - 50))
        {
            QVector2D
                    //VETOR gol -> bola  =>
                    Vgb = QVector2D(goalPoint - field.ball.ptGetPosition()),

                    //VETOR robo-> bola
                    Vrb = QVector2D(field.opponents.robots.value(1)->ptGetPosition() - field.ball.ptGetPosition()),

                    //VETOR distancia

                    d = Vrb - (Vrb.dotProduct(Vrb, Vgb) / Vgb.lengthSquared()) * Vgb;

            //Raio do Robo
            int Rrobo = 90;

            //------------- JOGADA TIME AMARELO ----------------

            //               MARCACAO POR ZONA

            field.opponents.robots.value(1)->vSetRollerSpeed(100);

        }


        //    QPoint ballPos = field.ball.ptGetPosition();

        //Jogador aliador 2
        field.allies.robots.value(2)->vMoveTo({-50, 0}, 0 , 1);
        field.allies.robots.value(2)->vSetRollerSpeed(100);
        field.allies.robots.value(2)-> bGetBallSensor();
        if ( field.allies.robots.value(2)->bGetBallSensor() == 1)
        {
            field.allies.robots.value(2)->vKick( 3, CHIP_KICK);
        }
        //    //JOgador aliado 3
        //     field.allies.robots.value(3)-> bGetBallSensor();
        //     field.allies.robots.value(3)->vMoveTo({2407, -1059}, M_PI , 2);

        //JOgador aliado 0
        field.allies.robots.value(0)-> bGetBallSensor();
        field.allies.robots.value(0)->vMoveTo({1554, 0050}, M_PI , 2);

        if(field.allies.robots.value(0)->bGetBallSensor() == 1)
        {//Pegar bola e abrir cruzada esquerda
            field.allies.robots.value(0)->vSetRollerSpeed(100);
            field.allies.robots.value(0)->vMoveTo({3009, 1210}, 0 , 2);
        }
        if(field.allies.robots.value(0)->ptGetPosition() == QPoint(3009, 1210))
        {
            field.allies.robots.value(0)->vMoveTo({3009, 1210}, -0.785 , 5);
            if(field.allies.robots.value(0)->fGetAngle() <= -0.700 &&
                    field.allies.robots.value(0)->fGetAngle() >= -0.8)
            {
                field.allies.robots.value(0)->vSetRollerSpeed(50);
                field.allies.robots.value(0)->vKick( 2.5, DIRECT_KICK);

            }
            //         field.allies.robots.value(3)->vMoveTo(field.ball.ptGetPosition() , field.ball.ptGetPosition() , 2);

        }
    }
    else
    {
        //-----------------------------------------Setando posicionamento inicial

        //GOLEIRO
        vSetRobotPosition(YELLOW_ID, 1, {4084 ,0000} );

        //ALAS
        vSetRobotPosition(YELLOW_ID, 0, {3260 ,1500} );
        //    vSetRobotPosition(YELLOW_ID, 0, {3260 ,1000} );
        vSetRobotPosition(YELLOW_ID, 2, {3260 ,-1000} );

        //FIXOS
        vSetRobotPosition(YELLOW_ID, 3, {2700 ,0650} );

        vSetRobotPosition(YELLOW_ID, 4, {2700 ,-0650} );

        //CAVALO / ZAGUEIRO
        vSetRobotPosition(YELLOW_ID, 5, {500,0000} );
    }


    //Procurar angulo de chute / Abertura

    //Limitar movimentacao com uso do roller(Lmax == 1)

    //    int idmais_prox()
    //    {


}
