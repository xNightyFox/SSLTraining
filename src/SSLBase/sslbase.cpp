#include "sslbase.h"
#include <cstdlib>

SSLBase::SSLBase()
{
    udpVision = new QUdpSocket();
    udpTeam = new QUdpSocket();

    udpVision->bind(QHostAddress::AnyIPv4, VISION_PORT,
                    QUdpSocket::ShareAddress);

    tmrLoop = new QTimer();
    connect(tmrLoop, &QTimer::timeout, this, &SSLBase::vUpdate);

    bPacketsReceived = false;
    iInterfaceIndex = 1;
    iCycles = 0;
}

SSLBase::~SSLBase()
{

}



void SSLBase::vDiscoverInterface()
{
    if(!bPacketsReceived && iCycles % 10)
    {
        udpVision->disconnect(SIGNAL(readyRead()));
        iInterfaceIndex++;
        vConnect();
    }
}

void SSLBase::vSendData()
{
    RobotControl control;

    for(const QSharedPointer<Robot> &robo : qAsConst(field.allies.robots))
    {
        // Adiciona um novo comando ao pacote `control`
        RobotCommand* command = control.add_robot_commands();
        MoveLocalVelocity* localVelocity = command->mutable_move_command()->mutable_local_velocity();

        command->set_id(robo->iGetID());

        const QVector3D robotCmd = robo->vt3dGetCommand();
        localVelocity->set_forward(robotCmd.y());
        localVelocity->set_left(-robotCmd.x()); // No GrSim é invertido
        localVelocity->set_angular(robotCmd.z());

        const ROBOT_KICK robotKick = robo->rkGetKick();
        command->set_kick_speed(robotKick.speed);
        command->set_kick_angle(robotKick.type == CHIP_KICK ? 45 : 0);
        command->set_dribbler_speed(robo->iGetRollerSpeed() * 100e3);
    }

    QByteArray datagram;
    datagram.resize(control.ByteSizeLong());
    control.SerializeToArray(datagram.data(), datagram.size());

    if(udpTeam->writeDatagram(datagram, haSimulatorIP, iTeamPort) == -1)
    {
        qWarning() << "[Warning] Erro ao enviar dados para os robôs! Erro: ("
                   << qPrintable(udpTeam->errorString()) << ")";
    }
}

void SSLBase::vUpdateVisionData(const SSL_WrapperPacket &_visionData)
{
    //Atualiza as informacoes dos robos e da bola
    //Aqui sera detectado se o robo saiu da visao ou entao, caso ele esteja no campo sera adicionada sua posicao atual
    //no vetor utilizado pelo kalman para calcular sua posicao real, o mesmo vale para a bola.
    if(_visionData.has_detection())
    {
        field.allies.vUpdateDetection(_visionData.detection());
        field.opponents.vUpdateDetection(_visionData.detection());
        field.ball.vUpdateFromDetection(_visionData.detection());
    }

    //Recebe os dados de geometria do campo
    if(_visionData.has_geometry())
    {
        field.geometry.vUpdateFromDetection(_visionData.geometry());
    }
}

void SSLBase::vConnect()
{
    QList<QNetworkInterface> availableInterfaces = QNetworkInterface::allInterfaces();
    if(iInterfaceIndex >= availableInterfaces.size())
    {
        qCritical() << "[Fatal] Não foram recebidos pacotes em nenhuma interface!";
        exit(-1);
    }

    const QNetworkInterface interface = availableInterfaces.at(iInterfaceIndex);

    if(udpVision->joinMulticastGroup(QHostAddress(VISION_IP), interface))
    {
        // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
        bPacketsReceived = false;
        connect(udpVision, &QUdpSocket::readyRead,
                this, &SSLBase::vParseVisionPacket);
        qInfo() << "[Info] Conectado! IP:Porta =" << VISION_IP << ":" << VISION_PORT
                << " | Interface:" << interface.humanReadableName();
    }
    else
    {
        qCritical() << "[Erro] Não foi possível se conectar com a visão! "
                    << "IP: " << VISION_IP << " | Interface: "
                    << interface.humanReadableName();
    }


    auto entryList = interface.addressEntries();
    if(entryList.empty())
    {
        qInfo() << "Nenhuma interface disponível";
        return;
    }
    QNetworkAddressEntry addrEntry = entryList.constFirst();
    haSimulatorIP = QHostAddress("255.255.255.255");//addrEntry.broadcast();
    iTeamPort = BLUE_PORT;

    if(field.allies.getTeamColor().iID != BLUE_ID)
    {
        iTeamPort = YELLOW_PORT;
    }

    qInfo() << QString("[Simulador] Conectando em %1:%2 @ %3")
        .arg(haSimulatorIP.toString())
        .arg(iTeamPort)
        .arg(interface.humanReadableName());

    // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
    connect(udpTeam, &QUdpSocket::readyRead,
            this, &SSLBase::vParseRobotFeedback);
}

void SSLBase::vStart()
{
    tmrLoop->start(CYCLE_MS);
    qInfo() << "[Info] Loop" << CYCLE_MS << "ms";
}

void SSLBase::vParseVisionPacket()
{
    if(!bPacketsReceived)
    {
        bPacketsReceived = true;
        qInfo() << "[Info] Pacote recebido!";
    }

    while(udpVision->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpVision->pendingDatagramSize());
        datagram.fill(0, udpVision->pendingDatagramSize());
        udpVision->readDatagram(datagram.data(), datagram.size());

        SSL_WrapperPacket visionData;
        visionData.ParseFromArray(datagram, datagram.size());

        if(visionData.ByteSizeLong() > 0)
        {
            vUpdateVisionData(visionData);
        }
    }
}

void SSLBase::vParseRobotFeedback()
{
    while(udpTeam->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpTeam->pendingDatagramSize());
        datagram.fill(0, udpTeam->pendingDatagramSize());
        udpTeam->readDatagram(datagram.data(), datagram.size());

        RobotControlResponse response;
        response.ParseFromArray(datagram, datagram.size());

        if(response.ByteSizeLong() > 0)
        {
            for(int i = 0; i < response.feedback_size(); ++i)
            {
                const RobotFeedback& feedback = response.feedback(i);
                if(feedback.has_id() && feedback.has_dribbler_ball_contact())
                {
                    const qint8 robotID = static_cast<qint8>(feedback.id());
                    field.allies.robots.value(robotID)->vSetBallSensor(feedback.dribbler_ball_contact());
                }
            }
        }
    }
}

void SSLBase::vUpdate()
{
    vDiscoverInterface();
    ++iCycles;

    vMain();

    vSendData();
}

//----------------------------------------------------------Funcoes Referentes Artilheiro Infinito

int SSLBase::verifica_Distancia_Robos(int ID_Robo, QPoint pos, float dist, int aliados){
    if(aliados == 1){
        if(QVector2D(field.allies.robots.value(ID_Robo)->ptGetPosition()).distanceToPoint(QVector2D(pos)) < dist){
            return 1;
        }
        else{
            return 0;
        }
    } else if (aliados == 0){
        if(QVector2D(field.opponents.robots.value(ID_Robo)->ptGetPosition()).distanceToPoint(QVector2D(pos)) < dist){
            return 1;
        }
        else{
            return 0;
        }
    }
}

int SSLBase::verifica_Goleiro(){
    if(verifica_Distancia_Robos(0, QPoint(4500, 0), 360, 0) == 1){
        qDebug() << "O GOLEIRO TÁ NA FRENTE PORRA";
        return 1;
    }
    else if (verifica_Distancia_Robos(0, QPoint(4500, 0), 360, 0) == 0){
        qDebug() << "CHUTA ESSE CARAIO";
        return 0;
    }
}

float SSLBase::alinha_robo(int ID_Robo, QPoint pos_Alinhamento){
    QVector2D vetor_A = QVector2D(pos_Alinhamento - QPoint(field.allies.robots.value(ID_Robo)->ptGetPosition()));
    QVector2D vetor_B = QVector2D(cosf(field.allies.robots.value(ID_Robo)->fGetAngle()), sinf(field.allies.robots.value(ID_Robo)->fGetAngle()));
    vetor_A.normalize();
    vetor_B.normalize();
    float cos_vetores = qRadiansToDegrees(qAcos(QVector2D::dotProduct(vetor_A, vetor_B)));
    qDebug() << cos_vetores;
    return cos_vetores;
}

void SSLBase::atacante_segue_bola(int ID_Robo)
{
    if(QPoint(field.ball.ptGetPosition()).x() >= 3500 &&
            QPoint(field.ball.ptGetPosition()).x() <= 4680 &&
            QPoint(field.ball.ptGetPosition()).y() <= 500 &&
            QPoint(field.ball.ptGetPosition()).y() >= -500)
    {
        qDebug() << "DENTRO DA ÁREA";
        field.allies.robots.value(ID_Robo)->vMoveTo(QPoint(field.allies.robots.value(ID_Robo)->ptGetPosition()), field.ball.ptGetPosition(), 2);
    }
    else
    {
        qDebug() << "FORA DA ÁREA";
        if(ja_Chutou == 1){
            field.allies.robots.value(ID_Robo)->vKick(0, DIRECT_KICK);
            ja_Chutou = 0;
        }
        field.allies.robots.value(ID_Robo)->vMoveTo(field.ball.ptGetPosition(), field.ball.ptGetPosition(), 2);
        if(QVector2D(field.allies.robots.value(ID_Robo)->ptGetPosition()).distanceToPoint(QVector2D(field.ball.ptGetPosition())) < 300)
        {
            qDebug() << "A menos de 300mm";
            field.allies.robots.value(ID_Robo)->vSetRollerSpeed(100);
            if (field.allies.robots.value(ID_Robo)->bGetBallSensor() == true)
            {
                qDebug() << "Robô sentiu a bolinha";
                if(robo_posicionou == 0)
                {
                    qDebug() << "AAAaaaaAAAA";
                    field.allies.robots.value(ID_Robo)->vMoveTo(field.allies.robots.value(ID_Robo)->ptGetPosition(), {4500, 0}, 1.5);
                    float angulo_Robo = alinha_robo(ID_Robo, QPoint(4500, 0));
                    if(angulo_Robo <= 2 && angulo_Robo >= 0)
                    {
                        if(verifica_Goleiro() == 0)
                        {
                            field.allies.robots.value(ID_Robo)->vKick(100, DIRECT_KICK);
                            ja_Chutou = 1;

                        }
                        else if (verifica_Goleiro() == 1)
                        {
                            field.allies.robots.value(ID_Robo)->vMoveTo(field.allies.robots.value(ID_Robo)->ptGetPosition(), {4500, -450}, 2.0);
                            robo_posicionou = 1;
                        }
                    }
                }
                else if (robo_posicionou == 1)
                {
                    if(robo_posicionou_2 == 0){
                        aleatorio = qrand() % 2;
                        qDebug() << aleatorio;
                        robo_posicionou_2 = 1;
                    }
                    if(aleatorio == 0 && robo_posicionou_2 == 1){
                        field.allies.robots.value(ID_Robo)->vMoveTo(field.allies.robots.value(ID_Robo)->ptGetPosition(), {4500, -480}, 2.0);

                        float angulo_Robo_2 = alinha_robo(ID_Robo, QPoint(4500, -480));

                        if(angulo_Robo_2 <= 2 && angulo_Robo_2 >= 0)
                        {
                            field.allies.robots.value(ID_Robo)->vKick(100, DIRECT_KICK);
                            ja_Chutou = 1;
                            robo_posicionou = 0;
                            robo_posicionou_2 = 0;
                        }
                    } else if(aleatorio == 1 && robo_posicionou_2 == 1){
                        field.allies.robots.value(ID_Robo)->vMoveTo(field.allies.robots.value(ID_Robo)->ptGetPosition(), {4500, 480}, 2.0);

                        float angulo_Robo_2 = alinha_robo(ID_Robo, QPoint(4500, 480));

                        if(angulo_Robo_2 <= 2 && angulo_Robo_2 >= 0)
                        {
                            field.allies.robots.value(ID_Robo)->vKick(6, DIRECT_KICK);
                            ja_Chutou = 1;
                            robo_posicionou = 0;
                            robo_posicionou_2 = 0;
                        }
                    }
                }
            }
        }
    }
}
