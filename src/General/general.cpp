#include "general.h"

General::General()
{

}

QVector2D General::vt2dRotateVector(const QVector2D &_vector, float _degrees)
{
    QVector2D rotated;

    _degrees = qDegreesToRadians(_degrees);

    rotated.setX(_vector.x() * qCos(_degrees) - _vector.y()*qSin(_degrees));
    rotated.setY(_vector.x() * qSin(_degrees) + _vector.y()*qCos(_degrees));

    return rotated;
}


QPoint General::vt2dRotateP2P1(const QPoint &_P1, const QPoint &_P2,
                                  float _degrees)
{
    QVector2D vector = QVector2D(_P2 - _P1);
    vector = General::vt2dRotateVector(vector, _degrees);
    return _P1 + vector.toPoint();
}

double General::dAngleV1V2(const QVector2D &_V1, const QVector2D &_V2)
{
    double angle = qAcos(_V1.dotProduct(_V1, _V2) / (_V1.length() * _V2.length()));
    angle *= (180.0/M_PI);

    if(qIsNaN(angle))
    {
        angle = 0;
    }
    return angle;
}
