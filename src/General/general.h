#ifndef GENERAL_H
#define GENERAL_H

#include <QVector2D>
#include <qmath.h>
#include <QDebug>

class General
{
public:
    General();

    static QVector2D vt2dRotateVector(const QVector2D &_vector, float _degrees);
    static QPoint vt2dRotateP2P1(const QPoint &_P1, const QPoint &_P2,
                                 float _degrees);
    static double dAngleV1V2(const QVector2D &_V1, const QVector2D &_V2);
};

#endif // GENERAL_H
