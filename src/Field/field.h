#ifndef FIELD_H
#define FIELD_H

#include "src/Field/ball.h"
#include "src/Field/geometry.h"
#include "src/General/general.h"
#include "src/Field/team.h"

class Field
{

public:
    SSLTeam allies;
    SSLTeam opponents;
    Ball ball;
    Geometry geometry;

    Field();
};

#endif // FIELD_H
