#ifndef BALL_H
#define BALL_H

#include <QPoint>
#include "src/General/general.h"
#include "ssl_vision_detection.pb.h"

class Ball
{
    QPoint ptPosition;

public:
    Ball();

    void vSetPosition(QPoint _pos);
    QPoint ptGetPosition() const;
    void vUpdateFromDetection(const SSL_DetectionFrame &_detection);
};

#endif // BALL_H
