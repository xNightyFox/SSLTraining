#ifndef TEAM_H
#define TEAM_H

#include <QHash>
#include <QColor>
#include <QString>
#include <QSharedPointer>
#include <QtGlobal>

#include "qdebug.h"
#include "src/Field/robot.h"
#include "src/General/general.h"

#include "ssl_vision_detection.pb.h"

#define BLUE_ID 1
#define YELLOW_ID -1

#define POSITIVE_SIDE 1
#define NEGATIVE_SIDE -1

#define MAX_ROBOTS 11

class TeamColor
{
public:
    QString strName;
    int iID;

    TeamColor(int _id, QString _name)
    {
        strName = _name;
        iID = _id;
    }

    ~TeamColor()
    {

    }
};

class SSLTeam
{
    int iSide;
    TeamColor tcColor;

public:
    QHash<int, QSharedPointer<Robot>> robots;

    SSLTeam();

    void vChangeColor(int _color, QString _name);
    int iGetSide() const;

    TeamColor getTeamColor() const;

    void vUpdateDetection(const SSL_DetectionFrame &_detection);
};

#endif // TEAM_H
