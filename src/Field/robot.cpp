#include "robot.h"

Robot::Robot(qint8 _id)
{
    iID = _id;
    bDetected = false;
    iMissedDetections = 0;

    ptPosition = OUT_OF_FIELD;
    fAngle = 0;

    fLinearKp = 0.003;
    fAngularKp = 5;

    vt3dCommand = QVector3D(0.0, 0.0, 0.0);
    kick.speed = 0;
    kick.type = DIRECT_KICK;
    iRollerSpeed = 0;
}

QVector2D Robot::vt2dDecomposeVelocity(const QVector2D &_vel)
{
    QVector2D decomposedVel;

    float anguloY = 0.0, anguloX = 0.0;
    float currentVel = _vel.length();

    QVector2D vetorXRobo, vetorYRobo(qCos(fAngle), qSin(fAngle));

    vetorXRobo = General::vt2dRotateVector(vetorYRobo, -90);
//    vetorXRobo = vetorXRobo - QVector2D(ptPosition);

    anguloX = General::dAngleV1V2(vetorXRobo, _vel)*M_PI/180;
    anguloY = General::dAngleV1V2(vetorYRobo, _vel)*M_PI/180;

    decomposedVel.setX(currentVel * qCos(anguloX));
    decomposedVel.setY(currentVel * qCos(anguloY));

    return decomposedVel;
}

float Robot::fPointToAngle(QPoint _pos)
{
    int _x = _pos.x() - ptPosition.x();
    int _y = _pos.y() - ptPosition.y();

    return qAtan2(_y, _x);
}

void Robot::vSetID(qint8 _id)
{
    iID = _id;
}

int Robot::iGetID() const
{
    return iID;
}

void Robot::vSetPosition(QPoint _pos)
{
    ptPosition = _pos;
}

QPoint Robot::ptGetPosition() const
{
    return ptPosition;
}

void Robot::vSetAngle(float _angle)
{
    fAngle = _angle;
}

float Robot::fGetAngle() const
{
    return fAngle;
}

void Robot::vSetCommand(QVector3D _cmd)
{
    vt3dCommand = _cmd;
}

QVector3D Robot::vt3dGetCommand() const
{
    return vt3dCommand;
}

void Robot::vKick(ROBOT_KICK _kick)
{
    kick = _kick;
}
void Robot::vKick(float _kickSpeed, KICK_TYPE _type)
{
    kick.speed = _kickSpeed;
    kick.type = _type;
}

ROBOT_KICK Robot::rkGetKick() const
{
    return kick;
}

void Robot::vSetRollerSpeed(int _speed)
{
    iRollerSpeed = _speed;
}

int Robot::iGetRollerSpeed() const
{
    return iRollerSpeed;
}

void Robot::vSetBallSensor(bool _sensor)
{
    bBallSensor = _sensor;
}

bool Robot::bGetBallSensor() const
{
    return bBallSensor;
}

void Robot::vMoveTo(QPoint _pos, float _angle, float _maxVelocity)
{
    const QVector2D error = QVector2D(_pos - ptPosition);
    float angleError = _angle - fAngle;

    if(qAbs(angleError) > M_PI)
    {
        if(_angle < fAngle)
        {
            angleError -= 2*M_PI;
        }
        else
        {
            angleError -= 2*M_PI;
        }
    }

    QVector2D cmd = vt2dDecomposeVelocity(fLinearKp * error);

    if(cmd.length() > _maxVelocity)
    {
        cmd = cmd.normalized() * _maxVelocity;
    }
    vt3dCommand = QVector3D(cmd, angleError * fAngularKp);
}

void Robot::vMoveTo(QPoint _pos, QPoint _anglePos, float _maxVelocity)
{
    vMoveTo(_pos, fPointToAngle(_anglePos), _maxVelocity);
}

void Robot::vSetKP(float _linear, float _angular)
{
    fLinearKp = _linear;
    fAngularKp = _angular;
}

void Robot::vStepDetection()
{
   iMissedDetections++;

   if(iMissedDetections >= PACKETS_TILL_MISSING && bDetected)
   {
       bDetected = false;
       ptPosition = OUT_OF_FIELD;
       qInfo() << "[Robô" << iID << "] Sumiu!";
   }
}

void Robot::vUpdateFromDetection(const SSL_DetectionRobot &_detection)
{
    if((int)_detection.robot_id() != iID)
    {
        return;
    }

    bDetected = true;
    iMissedDetections = 0;

    fAngle = _detection.orientation();
    ptPosition = QPoint(_detection.x(), _detection.y());
}
