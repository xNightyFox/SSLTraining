#include "team.h"

SSLTeam::SSLTeam() : tcColor(BLUE_ID, "Blue")
{
    iSide = NEGATIVE_SIDE;
    for(int n = 0; n < MAX_ROBOTS; ++n)
    {
        robots.insert(n, QSharedPointer<Robot>::create(n));
    }
}

void SSLTeam::vChangeColor(int _color, QString _name)
{
    tcColor = TeamColor(_color, _name);
}

int SSLTeam::iGetSide() const
{
    return iSide;
}

TeamColor SSLTeam::getTeamColor() const
{
    return tcColor;
}

void SSLTeam::vUpdateDetection(const SSL_DetectionFrame &_detection)
{
    SSL_DetectionRobot robotData;
    qint8 detectedRobots = _detection.robots_yellow_size();

    if(tcColor.iID == BLUE_ID)
    {
        detectedRobots = _detection.robots_blue_size();
    }

    for(qint8 n = 0; n < detectedRobots; ++n)
    {
        if(tcColor.iID == BLUE_ID)
        {
            robotData = _detection.robots_blue(n);
        }
        else
        {
            robotData = _detection.robots_yellow(n);
        }

        if(robotData.robot_id() >= MAX_ROBOTS)
        {
            qWarning() << "[Visao] ID fora do limite máximo! ID = "
                       << robotData.robot_id();
            return;
        }

        robots.value(robotData.robot_id())->vUpdateFromDetection(robotData);
    }

    for(QSharedPointer<Robot> bot : robots)
    {
        bot->vStepDetection();
    }
}
