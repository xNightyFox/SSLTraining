#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <QSize>
#include <QRect>
#include <QPoint>
#include <QDebug>

#include "ssl_vision_geometry.pb.h"

class Geometry
{
    QRect retField;
    QRect retPositiveGoal;
    QRect retNegativeGoal;

public:
    Geometry();

    void vSetField(int _width, int _height);
    void vSetGoals(int _width, int _height);

    QRect retGetField() const;
    QRect retGetPositiveGoal() const;
    QRect retGetNegativeGoal() const;

    void vUpdateFromDetection(const SSL_GeometryData &_detection);
};

#endif // GEOMETRY_H
