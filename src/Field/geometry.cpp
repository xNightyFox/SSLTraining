#include "geometry.h"

Geometry::Geometry()
{
    retField = retPositiveGoal = retNegativeGoal = QRect(0, 0, 0, 0);
}

void Geometry::vSetField(int _width, int _height)
{
    retField = QRect(-_width/2.0, _height/2.0,
                     _width, -_height);
}

void Geometry::vSetGoals(int _width, int _height)
{
    const QSize field = retField.size();
    QPoint positiveCorner(field.width()/2.0, _height/2.0);
    retPositiveGoal = QRect(positiveCorner, QSize(_width, -_height));

    QPoint negativeCorner(-field.width()/2 - _width, _height/2.0);
    retNegativeGoal = QRect(negativeCorner, QSize(_width, -_height));
}

QRect Geometry::retGetField() const
{
    return retField;
}

QRect Geometry::retGetPositiveGoal() const
{
    return retPositiveGoal;
}

QRect Geometry::retGetNegativeGoal() const
{
    return retNegativeGoal;
}

void Geometry::vUpdateFromDetection(const SSL_GeometryData &_detection)
{
    const SSL_GeometryFieldSize& fieldGeometry = _detection.field();

    //Atualiza o tamanho do campo
    if(retField.size() != QSize(fieldGeometry.field_length(), fieldGeometry.field_width()))
    {
        vSetField(fieldGeometry.field_length(), fieldGeometry.field_width());
    }

    //Dimensoes do gol
    vSetGoals(fieldGeometry.goal_depth(), fieldGeometry.goal_width());
}
