#ifndef ROBOT_H
#define ROBOT_H

#include <QPoint>
#include <QVector2D>
#include <QVector3D>
#include <qmath.h>

#include "src/General/general.h"
#include "ssl_vision_detection.pb.h"

typedef enum KICK_TYPE
{
    DIRECT_KICK = 0,
    CHIP_KICK = 1
}KICK_TYPE;

typedef struct ROBOT_KICK
{
    KICK_TYPE type;
    float speed;
}ROBOT_KICK;

constexpr QPoint OUT_OF_FIELD = QPoint(INT32_MAX, INT32_MAX);
#define PACKETS_TILL_MISSING 30

class Robot
{
    qint8 iID;
    QPoint ptPosition;
    float fAngle;

    float fLinearKp;
    float fAngularKp;
    QVector3D vt3dCommand;
    ROBOT_KICK kick;
    int iRollerSpeed;
    bool bBallSensor;

    bool bDetected;
    int iMissedDetections;

    QVector2D vt2dDecomposeVelocity(const QVector2D &_vel);
    float fPointToAngle(QPoint _pos);

public:
    Robot(qint8 _id = -1);

    void vSetID(qint8 _id);
    int iGetID() const;
    void vSetPosition(QPoint _pos);
    QPoint ptGetPosition() const;
    void vSetAngle(float _angle);
    float fGetAngle() const;

    void vSetCommand(QVector3D _cmd);
    QVector3D vt3dGetCommand() const;

    void vKick(ROBOT_KICK _kick);
    void vKick(float _kickSpeed, KICK_TYPE _type);
    ROBOT_KICK rkGetKick() const;

    void vSetRollerSpeed(int _speed);
    int iGetRollerSpeed() const;

    void vSetBallSensor(bool _sensor);
    bool bGetBallSensor() const;

    //POSICAO, ANGULO FRONTAL, VELOCIDADE DO ROBO
    void vMoveTo(QPoint _pos, float _angle, float _maxVelocity);
    void vMoveTo(QPoint _pos, QPoint _anglePos, float _maxVelocity);
    /**
     * @brief Utilize esta função para calibrar a movimentação do robô.
     *
     * @param _linear - Ganho da movimentação linear.
     * @param _angular - Ganho da movimentação angular.
     */
    void vSetKP(float _linear, float _angular);

    void vStepDetection();
    void vUpdateFromDetection(const SSL_DetectionRobot &_detection);
};

#endif // ROBOT_H
