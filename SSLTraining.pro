QT += core gui serialport network

CONFIG += c++14 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        src/Field/ball.cpp \
        src/Field/field.cpp \
        src/Field/geometry.cpp \
        src/Field/robot.cpp \
        src/Field/team.cpp \
        src/General/general.cpp \
        src/SSLBase/sslbase.cpp \
        src/SSLBase/ssltraining.cpp

include(include/protofiles/proto_compile.pri)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

message($$PWD)

HEADERS += \
    google/protobuf/any.h \
    google/protobuf/any.pb.h \
    google/protobuf/api.pb.h \
    google/protobuf/arena.h \
    google/protobuf/arena_impl.h \
    google/protobuf/arenastring.h \
    google/protobuf/arenaz_sampler.h \
    google/protobuf/descriptor.h \
    google/protobuf/descriptor.pb.h \
    google/protobuf/descriptor_database.h \
    google/protobuf/duration.pb.h \
    google/protobuf/dynamic_message.h \
    google/protobuf/empty.pb.h \
    google/protobuf/endian.h \
    google/protobuf/explicitly_constructed.h \
    google/protobuf/extension_set.h \
    google/protobuf/extension_set_inl.h \
    google/protobuf/field_access_listener.h \
    google/protobuf/field_mask.pb.h \
    google/protobuf/generated_enum_reflection.h \
    google/protobuf/generated_enum_util.h \
    google/protobuf/generated_message_bases.h \
    google/protobuf/generated_message_reflection.h \
    google/protobuf/generated_message_tctable_decl.h \
    google/protobuf/generated_message_tctable_impl.h \
    google/protobuf/generated_message_util.h \
    google/protobuf/has_bits.h \
    google/protobuf/implicit_weak_message.h \
    google/protobuf/inlined_string_field.h \
    google/protobuf/map.h \
    google/protobuf/map_entry.h \
    google/protobuf/map_entry_lite.h \
    google/protobuf/map_field.h \
    google/protobuf/map_field_inl.h \
    google/protobuf/map_field_lite.h \
    google/protobuf/map_type_handler.h \
    google/protobuf/message.h \
    google/protobuf/message_lite.h \
    google/protobuf/metadata.h \
    google/protobuf/metadata_lite.h \
    google/protobuf/parse_context.h \
    google/protobuf/port.h \
    google/protobuf/port_def.inc \
    google/protobuf/port_undef.inc \
    google/protobuf/reflection.h \
    google/protobuf/reflection_internal.h \
    google/protobuf/reflection_ops.h \
    google/protobuf/repeated_field.h \
    google/protobuf/repeated_ptr_field.h \
    google/protobuf/service.h \
    google/protobuf/source_context.pb.h \
    google/protobuf/struct.pb.h \
    google/protobuf/text_format.h \
    google/protobuf/timestamp.pb.h \
    google/protobuf/type.pb.h \
    google/protobuf/unknown_field_set.h \
    google/protobuf/wire_format.h \
    google/protobuf/wire_format_lite.h \
    google/protobuf/wrappers.pb.h \
    src/Field/ball.h \
    src/Field/field.h \
    src/Field/geometry.h \
    src/Field/robot.h \
    src/Field/team.h \
    src/General/general.h \
    src/SSLBase/sslbase.h

DISTFILES += \
    google/protobuf/any.proto \
    google/protobuf/api.proto \
    google/protobuf/descriptor.proto \
    google/protobuf/duration.proto \
    google/protobuf/empty.proto \
    google/protobuf/field_mask.proto \
    google/protobuf/source_context.proto \
    google/protobuf/struct.proto \
    google/protobuf/timestamp.proto \
    google/protobuf/type.proto \
    google/protobuf/wrappers.proto

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/release/ -lprotobuf
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/debug/ -lprotobuf
else:macx: LIBS += -L$$PWD/../../../../usr/local/Cellar/protobuf/21.12/lib -lprotobuf
else:unix: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/ -lprotobuf

INCLUDEPATH += $$PWD/../../../../usr/local/Cellar/protobuf/21.12/include
DEPENDPATH += $$PWD/../../../../usr/local/Cellar/protobuf/21.12/include

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/release/libprotobuf.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/debug/libprotobuf.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/release/protobuf.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/debug/protobuf.lib
else:macx: PRE_TARGETDEPS += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/libprotobuf.dylib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../../../usr/local/Cellar/protobuf/21.12/lib/libprotobuf.a
