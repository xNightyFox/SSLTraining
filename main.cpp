#include <QCoreApplication>
#include "src/SSLBase/sslbase.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    SSLBase ssl;
    ssl.vConnect();
    ssl.vStart();

    return a.exec();
}
